## OID Refs

- [1.3.6.1.4.1.52894](https://github.com/chez14/oid).**202108** → Verifyable Identifier
  - 1 → Issuer Identity
    - 1 → Issuer ID
    - 2 → Issuer legal name
    - 3 → Issuer branding name (should be just on CN?)
  - 2 → Issuer Scopes (able to issue certain types of card, we put this on `purpose` field on certificate field)
    - 1 → `marriage`
    - 2 → `individual` (nametag(?))

